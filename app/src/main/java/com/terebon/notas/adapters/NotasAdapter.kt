package com.terebon.notas.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.terebon.notas.R
import com.terebon.notas.entities.Nota

class NotasAdapter (private  val context: Context,
                    private val dataSource: ArrayList<Nota>
                    ) : BaseAdapter () {


    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return dataSource[position].id.toLong()
    }

    override fun getCount(): Int {
        return dataSource.count()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val nota = dataSource[position]

        val rowView = inflater.inflate(R.layout.activity_notes, parent, false)

        var TituloTextView = rowView.findViewById<TextView>(R.id.TituloTextView)
        var DescrTextView = rowView.findViewById<TextView>(R.id.DescrTextView)

        TituloTextView.setText(nota.titulo)
        DescrTextView.setText(nota.descripcion)

        return rowView

    }


    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

}



