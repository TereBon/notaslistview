package com.terebon.notas.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.terebon.notas.R
import com.terebon.notas.R.id.notasListView
import com.terebon.notas.adapters.NotasAdapter
import com.terebon.notas.entities.Nota
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    private var notas = ArrayList<Nota>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setDataSource()
        setListViewData()

        notasListView.setOnItemClickListener { parent, view, position, id ->
            var nota = notas[position]



            Toast.makeText(this, "Elemento seleccionado: " + nota.titulo, Toast.LENGTH_LONG).show()

            val intent = Intent(this, NotesActivity ::class.java)

            intent.putExtra("TituloTextView", nota.titulo)
            intent.putExtra("DescrTextView", nota.descripcion)

            startActivity(intent)

        }

    }

    private fun setDataSource() {
        notas.add(Nota(1, "titulo 1", "descr 1"))
        notas.add(Nota(2, "titulo 2", "descr 2"))
        notas.add(Nota(3, "titulo 3", "descr 3"))
        notas.add(Nota(4, "titulo 4", "descr 4"))
        notas.add(Nota(5, "titulo 5", "descr 5"))
    }


    private fun setListViewData() {

        // var adapter = NotasAdapter(baseContext, notas)


        var adapter = NotasAdapter(this, notas)



        notasListView.adapter = adapter
        notasListView.deferNotifyDataSetChanged()


    }

}

