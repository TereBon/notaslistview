package com.terebon.notas.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.terebon.notas.R

class NotesActivity : AppCompatActivity() {

    lateinit var titulo : TextView
    lateinit var descripcion : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notes)

        titulo = findViewById(R.id.TituloTextView)
        descripcion = findViewById(R.id.DescrTextView)

        var myIntent : Intent = intent

        var tvTitulo = myIntent.getStringExtra("TituloTextView")
        var tvDescripcion = myIntent.getStringExtra("DescrTextView")



        titulo.text = tvTitulo
        descripcion.text = tvDescripcion


    }

    fun setNotes () {

    }
}
